import React, { Props } from 'react';
import { APP_ROUTES } from '../../models/RoutingPaths';
import LabelRequired from '../common/LabelRequired';
import { RouteComponentProps } from 'react-router';


interface HomeState {
    username: string;
    password: string;
    confirm_password: string;
    button_indicator: boolean;
    error_msg: string;
}

interface RegistrationProps {

}


class Registration extends React.Component<RegistrationProps,HomeState> {

    constructor(props: any) {
        super(props);

        this.state = {
            username: "",
            password: "",
            confirm_password: "",
            button_indicator: true,
            error_msg: "",
        }
    }

    componentDidMount() {

    }

    
    passwordChecker = (keys: any) => {
        let self = this;
        this.setState(keys);
        let errorMsg = "";
        let btnIndicator = true;
        setTimeout(function(){  
            if (self.state.username && self.state.username.length < 8) {
                btnIndicator = true;
                errorMsg = "Username must have 8 characters or more";
            } 
            if (self.state.password && self.state.password.length < 8) {
                btnIndicator = true;
                errorMsg = "Password must have 8 characters or more";
            } 
            if (self.state.confirm_password && self.state.confirm_password.length < 8) {
                btnIndicator = true;
                errorMsg = "Confirm Password must have 8 characters or more";
            } 
            if (self.state.password.length >= 8 && self.state.confirm_password.length >= 8) {
                if (self.state.password && self.state.confirm_password) {
                    if (self.state.password === self.state.confirm_password) {
                        btnIndicator = false;
                        errorMsg = "";
                    }
                    else {
                        btnIndicator = true;
                        errorMsg = "Password and confirm password doesn't matched!";
                    }
                }
            }

            self.setState({ button_indicator: btnIndicator, error_msg: errorMsg });
        }, 300);
        
    }
    
    submitDetails = () => {
        let username = this.state.username;
        let password = this.state.password;
        let confirmpassword = this.state.confirm_password;
        console.log('username:',username,"------",'password:',password,"-------","confirm password:",confirmpassword);
        alert('username:'+username+"------"+'password:'+password+"-------"+"confirm password:"+confirmpassword);
    }
    
    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <div className="card mt-5">
                        <h5 className="card-header">Registration</h5>
                            <div className="card-body">                  
                                <div className="container">
                                    <div className="container-fliud">

                                        {this.state.error_msg.length > 0 && 
                                            <div className="row">
                                                <div className="alert alert-danger border">{this.state.error_msg}</div>
                                        </div>}
        
                                        <div className="row">
                                            <div className="col-md-4 text-left mb-3">
                                                <LabelRequired labelText={'Username'} htmlFor="text-word" required={true} />
                                            </div>
                                            <div className="col-md-8 text-left">
                                                <input type="text" className="" name="username" onChange={(e)=> { this.passwordChecker({...this.state, username: e.target.value }); }} />
                                            </div>
                                            <div className="col-md-4 text-left mb-3">
                                                <LabelRequired labelText={'Password'} htmlFor="text-word" required={true} />
                                            </div>
                                            <div className="col-md-8 text-left">
                                                <input type="password" className="" name="password" onChange={(e)=> { this.passwordChecker({...this.state, password: e.target.value }); } }/>
                                            </div>
                                            <div className="col-md-4 text-left mb-3">
                                                <LabelRequired labelText={'Confirm Password'} htmlFor="text-word" required={true} />
                                            </div>
                                            <div className="col-md-8 text-left">
                                                <input type="password" className="" name="confirm_password" onChange={(e)=> { this.passwordChecker({...this.state, confirm_password: e.target.value }); } }/>
                                            </div>
                                            <div className="col-md-4">&nbsp;</div>
                                            <div className="col-md-8 text-left">
                                                <button className="btn btn-primary" disabled={this.state.button_indicator} onClick={(e)=>{ this.submitDetails() }}>Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
           
        )
    }



}



export default Registration;
